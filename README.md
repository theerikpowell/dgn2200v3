# Netgear DGN2200V3 ADSL Modem
## Origins
The device is used to connect to dsl and was gifted to me by my Grandmother 
when the device stopped working after a lightning storm. With the replacement 
cost being what it was, she decided "I don't need no damn internet anymore".


I wasn't particularly interested in the device, but being in IT, I have grown
quite used to my role as a technology garbage collector, and I figured I may
find it useful somehow.  


Fast Forward several years, and we have a shitty internet connection. It's fast,
just not reliable. Nothing is more rage enducing during a video game, like being
disconnected 4-5 times in a row because the modem is configured to reboot when
it experiences a T3 timeout error. (WHO THOUGHT THAT WAS A GOOD IDEA!!) I'm not 
the hardware engineer, so maybe there is a purpose, but it is very enfuriating.

## The Useful Somehow
Now I live in a city. Not a large metropolis, but close enough to several 
businesses that offer free wifi, and my ASUS n66u router can already handle 
multiple connections to the internet with the ability to load balance or 
failover. 


Suddenly the useful somehow comes into focus. If I can boost the signal strength
of the Netgear modem enough to reach one of the free wifi connections, I can set
it up as a bridge to the free wifi. The target network is on a different ISP, so
the possibility of both being down at the same time, is, well, better than you 
think given the region. Honestly, we are one backhoe away from the South Park 
episode about the Internet going down. 

## Steps to bridge the Gap (Descriptive)

## Steps to bridge the Gap (Technical)
Useful Link: http://www.evolware.org/?p=114

### Device Details
Model: DGN2200v3
Firmware: ????

``` This will hard reset the device and flash back to the default firmware settings. ```
1. Unbrick the device!
  1. Look for the reset button on the bottom of the device.
  1. Press and hold until the power button blinks twice.

``` This will enable debug mode, and turn on telnet ```
1. Enable Telnet
  1. Open the Router admin page
  1. Login using admin/password
  1. Navigate to http://[Router IP Address]/setup.cgi?todo=debug

``` This will scan for wireless network information and list the available networks. ```
1. Using putty, open [Router IP Address] using telnet
1. When prompted, enter admin as the username.
1. When prompted, ener password as the password.
1. At the bash prompt, enter `wlctl scan`, to scan for wireless network information.
1. To view the results, enter `wlctl scanresults`.


## Turn it into a bridge
